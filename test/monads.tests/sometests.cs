using Xunit;
using Monads;
using FluentAssertions;

// Note: we need to use:
//    dnvm use -r coreclr -arch x64 1.0.0-rc1-update1
// because xunit dnx runner have no been updated to rc2.

namespace Monads.Tests
{
    public class SomeTests
    {
        [Fact]
        public void QuickSort()
        {
            var expected = new [] {1,3,4,4,7};
            var actual = SortHelpers.QuickSort(new [] {1,4,3,7,4});

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void QuickSort2()
        {
            SortHelpers.QuickSort2(new[] { 1, 4, 3, 7, 4 }).Should().Equal(1, 3, 4, 4, 7);
        }

        [Fact]
        public void Test()
        {
            var expected = 2m;
            var actual = 10m / 5m;

            actual.Should().Be(expected);
        }
        
        [Fact]
        public void Boolean() 
        {
            new True().Not().Should().BeOfType<False>();
            new False().Not().Should().BeOfType<True>();
            
            new False().Or(new False()).Should().BeOfType<False>();
            new False().Or(new True()).Should().BeOfType<True>();
            new True().Or(new False()).Should().BeOfType<True>();
            new True().Or(new True()).Should().BeOfType<True>();
            
            new False().And(new False()).Should().BeOfType<False>();
            new False().And(new True()).Should().BeOfType<False>();
            new True().And(new False()).Should().BeOfType<False>();
            new True().And(new True()).Should().BeOfType<True>();
            
        }
    }
}