using System;

namespace Monads 
{
    public static class Examples 
    {
        public static void Execute() 
        {
            Func<int, Nullable<double>> log = x => x > 0 
                ? new Nullable<double>(Math.Log(x)) 
                : new Nullable<double>();
            
            Func<double, Nullable<decimal>> toDecimal = y =>
            { 
                var ydec = (decimal)y;
                return Math.Abs(ydec) < decimal.MaxValue 
                    ? new Nullable<decimal>(ydec) 
                    : new Nullable<decimal>(); 
            };
            
            Func<int, Nullable<decimal>> both = NullableMonad.ComposeSpecial(log, toDecimal);
            
            Console.Write($"log({3}) = {both(3)}");        
        }
    }
    
    // TODO: Create Lazy, Nullable, IEnumerable, Task
    
    // Func<T>
    public delegate T OnDemand<T>();
    
    public static class OnDemandMonad 
    {
        static OnDemand<T> CreateSimple<T>(T val)
        {
            return () => val; // wrap
        }
        
        static OnDemand<R> ApplySpecialFunction<A, R>(
            OnDemand<A> onDemand,
            Func<A, OnDemand<R>> fn)
        {
            // unwrap is done inside the 
            return () => 
            {
                A a = onDemand(); // unwrap
                OnDemand<R> onDemandOfB = fn(a); // transform to OnDemand
                return onDemandOfB(); // unwrap (or else caller need to unwrap twice)
            };
        }
    }
    
    public static class NullableMonad
    {
        static Nullable<T> CreateSimple<T>(T t) where T:struct
        {
            return new Nullable<T>(t);
        }
        
        // Select in Linq
        public static Nullable<R> ApplyFunction<A, R>(
            Nullable<A> nullable,
            Func<A, R> fn) where A:struct where R:struct
        {
            // unwrap, transform, wrap
            return nullable.HasValue 
                ? new Nullable<R>(fn(nullable.Value)) 
                : new Nullable<R>(); 
        }
        
        // SelectMany in Linq
        public static Nullable<R> ApplySpecialFunction<A, R>(
            Nullable<A> nullable,
            Func<A, Nullable<R>> fn) 
        where A:struct where R:struct
        {
            return nullable.HasValue 
                ? fn(nullable.Value)
                : new Nullable<R>();
        }
        
        public static Func<X, Nullable<Z>> ComposeSpecial<X, Y, Z>(
            Func<X, Nullable<Y>> f,
            Func<Y, Nullable<Z>> g) where Y:struct where Z:struct
        { 
            return x => ApplySpecialFunction(f(x), g);
        }
    }
    
}