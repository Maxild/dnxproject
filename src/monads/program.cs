using System;

namespace Monads 
{
    public class Program 
    {
        public static int Main() {
            Console.WriteLine("Starting program...");
            Examples.Execute();
            return 0;
        }
    }

}