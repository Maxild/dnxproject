namespace Monads 
{
    public abstract class Boolean 
    {
        public abstract Boolean Not();
        public abstract Boolean And(Boolean other);
        public abstract Boolean Or(Boolean other);
    }
    
    public sealed class False : Boolean 
    {
        public override Boolean Not() => new True();
        public override Boolean And(Boolean _) => this;
        public override Boolean Or(Boolean other) => other;
    }
    
    public sealed class True : Boolean
    {
        public override Boolean Not() => new False();
        public override Boolean And(Boolean other) => other;
        public override Boolean Or(Boolean _) => this;
    }
}