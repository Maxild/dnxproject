using System;
using System.Collections.Generic;
using System.Linq;

namespace Monads
{
    public static class SortHelpers
    {
        // Try write this in Haskell using pattern matching and list comprehension
        public static IEnumerable<T> QuickSort<T>(IEnumerable<T> values)
            where T : IComparable<T>
        {
            var rest = values.Skip(1);

            if (false == rest.Any())
            {
                // single value..nothing to sort
                return values;
            }

            T pivot = values.First();

            // list comprehension in c#
            var left = from x in rest
                where x.CompareTo(pivot) <= 0
                select x;

            var right = from x in rest
                where x.CompareTo(pivot) > 0
                select x;

            return QuickSort(left).Concat(new [] {pivot}).Concat(QuickSort(right));
        }

        public static IEnumerable<T> QuickSort2<T>(IEnumerable<T> values)
            where T : IComparable<T>
        {
            var ps = values.Take(1);
            var pt = from pivot in ps
                    from x in values.Skip(1)
                    group x by 0 < x.CompareTo(pivot);

            var lt = pt.Where(g => !g.Key).SelectMany(g => QuickSort(g));
            var gt = pt.Where(g => g.Key).SelectMany(g => QuickSort(g));

            return lt.Concat(ps).Concat(gt);
        }
    }

}